package work.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import work.persistence.model.TextExercises;

import java.util.List;

public interface TextExercisesRepository extends JpaRepository<TextExercises, Long> {
    TextExercises findFirstByUserIdAndModuleIdAndSectionId(Long userId, int moduleId, int sectionId);
    List<TextExercises> findAllByUserIdAndModuleId(Long userId, int moduleId);
}





