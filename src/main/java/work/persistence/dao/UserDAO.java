package work.persistence.dao;

import work.persistence.model.User;

import java.util.List;

public interface UserDAO {

    List<User> get();

    User get(int id);

    void save(User user);



}
