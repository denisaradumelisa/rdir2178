package work.persistence.dao;

import org.springframework.data.repository.CrudRepository;
import work.persistence.model.TextExercises;

public interface ITextExercisesRepository extends CrudRepository<TextExercises, Integer> {

        TextExercises findByUserId(int userId);

}
