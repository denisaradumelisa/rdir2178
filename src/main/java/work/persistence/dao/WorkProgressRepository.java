package work.persistence.dao;

import work.persistence.model.WorkProgress;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WorkProgressRepository extends JpaRepository<WorkProgress, Long> {
    List<WorkProgress> findAllById(Long id);
}
