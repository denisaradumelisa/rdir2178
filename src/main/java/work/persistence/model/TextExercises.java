package work.persistence.model;

import javax.persistence.*;


@Entity
@Table(name = "textExercises")
public class TextExercises {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name="moduleId")
    private int moduleId;

    @Column(name="sectionId")
    private int sectionId;

    @Column(name="content")
    private String content;


    @Column(name="userId")
    private Long userId;

    //@OneToMany(mappedBy="textexercises")
    //Set<User> users;

    //private List<User> users;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public int getSectionId() {
        return sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

 //   public List<User> getUsers() {
//        return users;
 //   }
  //  public void setPhotos(List<User> users) {
   //     this.users = users;
  //  }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return id + " " + sectionId + " " + moduleId + " " + content + " " + userId;
    }

    @Override
    public boolean equals(Object obj) {
        // assume they don't match.
        boolean returnValue = false;
        if (obj instanceof TextExercises) {
            TextExercises incomingSpecimen = (TextExercises) obj;
            returnValue = incomingSpecimen.getId() == getId();
        }
        return returnValue;
    }


}
















