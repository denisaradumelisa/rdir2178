package work.persistence.model;

import javax.persistence.*;


@Entity
@Table(name = "tableExercises")
public class TableExercises {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String weekDay;

    @Column
    private String description;

    @Column
    private String emotionReason;

    @Column
    private EmotionValue emotionValue;

    @Column
    private Long userId;

    public TableExercises(String weekDay, String description, String emotionReason, EmotionValue emotionValue, Long userId) {
        this.weekDay = weekDay;
        this.description = description;
        this.emotionReason = emotionReason;
        this.emotionValue = emotionValue;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmotionReason() {
        return emotionReason;
    }

    public void setEmotionReason(String emotionReason) {
        this.emotionReason = emotionReason;
    }

    public EmotionValue getEmotionValue() {
        return emotionValue;
    }

    public void setEmotionValue(EmotionValue emotionValue) {
        this.emotionValue = emotionValue;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
















