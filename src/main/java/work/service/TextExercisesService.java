package work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import work.dto.TextExercisesListDTO;
import work.persistence.dao.TextExercisesRepository;
import work.persistence.model.TextExercises;

import javax.annotation.PostConstruct;
import java.util.*;

@Component
public class TextExercisesService {
    @Value("${firstModuleSections}")
    int firstModuleSections;
    @Value("${secondModuleSections}")
    int secondModuleSections;
    @Value("${thirdModuleSections}")
    int thirdModuleSections;
    HashMap<Integer, Integer> MODULE_SECTION_MAP = new HashMap<>();

    @Autowired
    TextExercisesRepository textExercisesRepository;

    @PostConstruct
    public void createMap() {
        MODULE_SECTION_MAP.put(1, firstModuleSections);
        MODULE_SECTION_MAP.put(2, secondModuleSections);
        MODULE_SECTION_MAP.put(3, thirdModuleSections);
    }


    public TextExercisesListDTO findTextExercise(Long userId, int moduleId) {
        TextExercisesListDTO textExercisesListDTO = new TextExercisesListDTO();
        textExercisesListDTO.addTextExerciseList(createPopulatedExercises(moduleId));
        for (int sectionId = 0; sectionId < MODULE_SECTION_MAP.get(moduleId); sectionId++) {
            TextExercises textExercise = textExercisesRepository.findFirstByUserIdAndModuleIdAndSectionId(userId, moduleId, sectionId);
            if (textExercise != null) {
                textExercisesListDTO.getTextExercises().set(sectionId, textExercise);
            }
        }
        return textExercisesListDTO;
    }

    public boolean moduleCompleted(Long userId, int moduleId){
        return MODULE_SECTION_MAP.get(moduleId)
                .equals(textExercisesRepository.findAllByUserIdAndModuleId(userId, moduleId).size());
    }

    private List<TextExercises> createPopulatedExercises(int moduleId) {
        List<TextExercises> textExercises = new ArrayList<>();
        for (int i = 0; i < MODULE_SECTION_MAP.get(moduleId); i++) {
            TextExercises textExercise = new TextExercises();
            textExercise.setModuleId(moduleId);
            textExercise.setSectionId(i);
            textExercises.add(textExercise);
        }
        return textExercises;
    }

    public void saveAll(TextExercisesListDTO textExercisesListDTO) {
        textExercisesListDTO.getTextExercises().forEach(this::save);
    }

    public void save(TextExercises textExercises) {
        TextExercises existingExercise;
        if (textExercises.getUserId() == null) {
            throw new RuntimeException("User ID is not found: " + textExercises.getUserId());
        }
        if (textExercises.getModuleId() == 0) {
            return;
        }
        existingExercise = textExercisesRepository.findFirstByUserIdAndModuleIdAndSectionId(textExercises.getUserId(), textExercises.getModuleId(), textExercises.getSectionId());
        if (existingExercise != null) {
            existingExercise.setContent(textExercises.getContent());
            textExercisesRepository.save(existingExercise);
        } else {
            textExercisesRepository.save(textExercises);
        }
    }


}
