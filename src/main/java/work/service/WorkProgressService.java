package work.service;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import work.dto.StepDTO;
import work.persistence.dao.UserRepository;
import work.persistence.dao.WorkProgressRepository;
import work.persistence.model.User;
import work.persistence.model.WorkProgress;

import java.util.LinkedList;
import java.util.List;

@Service
public class WorkProgressService {
    @Value("${firstModuleName}")
    String firstModuleName;
    @Value("${secondModuleName}")
    String secondModuleName;
    @Value("${thirdModuleName}")
    String thirdModuleName;

    @Value("${firstModuleSections}")
    String firstModuleSections;
    @Value("${secondModuleSections}")
    String secondModuleSections;
    @Value("${thirdModuleSections}")
    String thirdModuleSections;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WorkProgressRepository workProgressRepository;
    @Autowired
    private TextExercisesService textExercisesService;
    private String ENABLED_MODULE_CLASS = "enabledModule";
    private String DISABLED_MODULE_CLASS = "disabledModule";
    private String ADMIN_ROLE = "ROLE_ADMIN";
    private String THERAPIST_ROLE = "ROLE_THERAPIST";

    public List<StepDTO> getWorkProgressForUserId(Long id) {
        User currentUser = userRepository.getOne(id);
        List<StepDTO> workProgressMap = createStaticWorkProgress();
        if (currentUser == null) {
            throw new ServiceException("could not find current user for id" + id);
        }
        if (currentUser.getRoles().contains(ADMIN_ROLE) ||
                currentUser.getRoles().contains(THERAPIST_ROLE)) {
            return createAdminWorkProgress();
        } else {
            List<WorkProgress> allWorkProgress = workProgressRepository.findAllById(currentUser.getId());
            if (allWorkProgress.size() == 0) {
                return workProgressMap;
            } else if (allWorkProgress.size() == 1) {
                workProgressMap.set(1, new StepDTO(2, secondModuleName, ENABLED_MODULE_CLASS));
            } else {
                workProgressMap.set(2, new StepDTO(3, thirdModuleName, ENABLED_MODULE_CLASS));
            }
            return workProgressMap;
        }
    }

    public void markWorkProgress(Long userId, int moduleId) {
        if (textExercisesService.moduleCompleted(userId, moduleId)) {
            WorkProgress workProgress = new WorkProgress();
            workProgress.setModuleId(moduleId + 1);
            workProgress.setUser(userRepository.getOne(userId));
            workProgressRepository.save(workProgress);
        }
    }

    private List<StepDTO> createStaticWorkProgress() {
        List<StepDTO> linkedList = new LinkedList<>();
        linkedList.add(new StepDTO(1, firstModuleName, ENABLED_MODULE_CLASS));
        linkedList.add(new StepDTO(2, secondModuleName, DISABLED_MODULE_CLASS));
        linkedList.add(new StepDTO(3, thirdModuleName, DISABLED_MODULE_CLASS));
        return linkedList;
    }

    private List<StepDTO> createAdminWorkProgress() {
        List<StepDTO> linkedList = new LinkedList<>();
        linkedList.add(new StepDTO(1, firstModuleName, ENABLED_MODULE_CLASS));
        linkedList.add(new StepDTO(2, secondModuleName, ENABLED_MODULE_CLASS));
        linkedList.add(new StepDTO(3, thirdModuleName, ENABLED_MODULE_CLASS));
        return linkedList;
    }
}
