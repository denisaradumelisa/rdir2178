package work.registration;

import java.util.Locale;

import work.persistence.model.User;
import org.springframework.context.ApplicationEvent;

@SuppressWarnings("serial")
public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private final String appUrl;
    private final Locale locale;
    private final User user;
    private final boolean mustSetPassword;

    public OnRegistrationCompleteEvent(final User user, final Locale locale, final String appUrl) {
        super(user);
        this.user = user;
        this.locale = locale;
        this.appUrl = appUrl;
        this.mustSetPassword = false;
    }

    public OnRegistrationCompleteEvent(final User user, final Locale locale, final String appUrl, boolean mustSetPassword) {
        super(user);
        this.user = user;
        this.locale = locale;
        this.appUrl = appUrl;
        this.mustSetPassword = mustSetPassword;
    }

    //

    public String getAppUrl() {
        return appUrl;
    }

    public Locale getLocale() {
        return locale;
    }

    public User getUser() {
        return user;
    }

    public boolean isMustSetPassword() {
        return mustSetPassword;
    }
}
