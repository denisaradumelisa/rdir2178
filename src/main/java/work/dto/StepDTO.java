package work.dto;

public class StepDTO {
    private int stepId;
    private String stepName;
    private String stepClass;

    public StepDTO(int stepId, String stepName, String stepClass) {
        this.stepId = stepId;
        this.stepName = stepName;
        this.stepClass = stepClass;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public String getStepClass() {
        return stepClass;
    }

    public void setStepClass(String stepClass) {
        this.stepClass = stepClass;
    }

    @Override
    public String toString() {
        return "StepDTO{" +
                "stepId=" + stepId +
                ", stepName='" + stepName + '\'' +
                ", stepClass='" + stepClass + '\'' +
                '}';
    }
}
