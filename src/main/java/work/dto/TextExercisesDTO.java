package work.dto;

public class TextExercisesDTO {
    private int moduleId;

    private int sectionId;

    private String content;

    private Long userId;

    public TextExercisesDTO() {
    }

    public TextExercisesDTO(int moduleId, int sectionId, String content, Long userId) {
        this.moduleId = moduleId;
        this.sectionId = sectionId;
        this.content = content;
        this.userId = userId;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public int getSectionId() {
        return sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "TextExercisesDTO{" +
                "moduleId=" + moduleId +
                ", sectionId=" + sectionId +
                ", content='" + content + '\'' +
                ", userId=" + userId +
                '}';
    }
}
