package work.dto;

import work.persistence.model.TextExercises;

import java.util.ArrayList;
import java.util.List;

public class TextExercisesListDTO {
    List<TextExercises> textExercises = new ArrayList<>();

    public TextExercisesListDTO() {
    }

    public TextExercisesListDTO(List<TextExercises> textExercises) {
        this.textExercises = textExercises;
    }

    public void addTetxExercise(TextExercises textExercises){
        this.textExercises.add(textExercises);
    }

    public void addTextExerciseList(List<TextExercises> textExercises){
        this.textExercises.addAll(textExercises);
    }

    public List<TextExercises> getTextExercises() {
        return textExercises;
    }

    public void setTextExercises(List<TextExercises> textExercises) {
        this.textExercises = textExercises;
    }
}
