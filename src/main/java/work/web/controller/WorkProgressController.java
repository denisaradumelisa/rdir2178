package work.web.controller;

import work.persistence.model.User;
import work.service.WorkProgressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WorkProgressController {
    @Autowired
    private WorkProgressService workProgressService;

    @RequestMapping(value = "/workProgress", method = RequestMethod.GET)
    public String getWorkProgress(final Model model,final Authentication authentication) {
        Long userId = ((User)authentication.getPrincipal()).getId();
        model.addAttribute("workProgress",workProgressService.getWorkProgressForUserId(userId));
            return "homepage";
    }
}
