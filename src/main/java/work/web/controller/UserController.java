package work.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import work.persistence.model.User;
import work.registration.OnRegistrationCompleteEvent;
import work.security.ActiveUserStore;
import work.service.IUserService;
import work.web.dto.RegisterUserForm;
import work.web.dto.UserDto;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@Controller
public class UserController extends BaseController {

    @Autowired
    ActiveUserStore activeUserStore;

    @Autowired
    IUserService userService;
    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private MessageSource messages;

    @RequestMapping(value = "/loggedUsers", method = RequestMethod.GET)
    public String getLoggedUsers(final Locale locale, final Model model) {
        model.addAttribute("users", activeUserStore.getUsers());
        return "users";
    }

    @RequestMapping(value = "/loggedUsersFromSessionRegistry", method = RequestMethod.GET)
    public String getLoggedUsersFromSessionRegistry(final Locale locale, final Model model) {
        model.addAttribute("users", userService.getUsersFromSessionRegistry());
        return "users";
    }

//trimite formularul
    @RequestMapping(value = "/registrationUsers", method = RequestMethod.POST)
    public String registrationUsers(@ModelAttribute(value = "registerUserForm") @Valid RegisterUserForm registerUserForm, final HttpServletRequest request) {
        UserDto user = new UserDto();
        user.setFirstName(registerUserForm.getFirstName());
        user.setLastName(registerUserForm.getLastName());
        user.setEmail(registerUserForm.getEmail());
        user.setPassword("no password");
         User registerUser =  userService.registerNewUserAccount(user);
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registerUser, request.getLocale(), getAppUrl(request), true));
        return "homepage";
    }

    //afiseaza formularul registration
    @RequestMapping(value = "/showRegistrationUsers", method = RequestMethod.GET)
    public String showRegistrationUsers( Model model) {
        model.addAttribute("registerUserForm",new RegisterUserForm());
        return "registrationUsers.html";
    }




}
