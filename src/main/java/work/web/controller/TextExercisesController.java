package work.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import work.dto.TextExercisesDTO;
import work.dto.TextExercisesListDTO;
import work.persistence.model.TextExercises;
import work.persistence.model.User;
import work.service.TextExercisesService;
import work.service.WorkProgressService;

import java.util.List;

@Controller
public class TextExercisesController {

    @Autowired
    private TextExercisesService textExercisesService;

    @Autowired
    private WorkProgressService workProgressService;

    @RequestMapping(value = "/saveExercises/{moduleId}", method = RequestMethod.POST)
    public String saveExercises(@PathVariable(value = "moduleId") int moduleId, @ModelAttribute TextExercisesListDTO textExercisesListDTO, final Authentication authentication) {
        Long userId = ((User) authentication.getPrincipal()).getId();
        textExercisesListDTO.getTextExercises().forEach(textExercises -> textExercises.setUserId(userId));
        textExercisesService.saveAll(textExercisesListDTO);
        workProgressService.markWorkProgress(userId,moduleId);
        return "redirect:/getTextExercises/" + moduleId;
    }

    @RequestMapping(value = "/getTextExercises/{moduleId}")
    public String getTextExercises(@PathVariable(value = "moduleId") int moduleId, Model model, final Authentication authentication) {
        Long userId = ((User) authentication.getPrincipal()).getId();
        TextExercisesListDTO textExerciseContents = textExercisesService.findTextExercise(userId, moduleId);
        model.addAttribute("textExercises", textExerciseContents);
        return "activitate";
    }


}
