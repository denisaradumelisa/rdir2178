package work.spring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import work.persistence.dao.PrivilegeRepository;
import work.persistence.dao.RoleRepository;
import work.persistence.dao.UserRepository;
import work.persistence.model.Privilege;
import work.persistence.model.Role;
import work.persistence.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = false;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    // API

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }

        // == create initial privileges
        final Privilege readPrivilege = createPrivilegeIfNotFound("READ_PRIVILEGE");
        final Privilege writePrivilege = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
        final Privilege passwordPrivilege = createPrivilegeIfNotFound("CHANGE_PASSWORD_PRIVILEGE");
        final Privilege therapistPrivilege = createPrivilegeIfNotFound("THERAPIST_PRIVILEGE");
        final Privilege childPrivilege = createPrivilegeIfNotFound("CHILD_PRIVILEGE");

        // == create initial roles -admin, terapist, user:child,parent
        final List<Privilege> adminPrivileges = new ArrayList<Privilege>(Arrays.asList(readPrivilege, writePrivilege, passwordPrivilege));
        final List<Privilege> childPrivileges = new ArrayList<Privilege>(Arrays.asList(readPrivilege, passwordPrivilege, childPrivilege));
        final List<Privilege> therapistPrivileges = new ArrayList<>(Arrays.asList(readPrivilege, passwordPrivilege, therapistPrivilege));
        final Role adminRole = createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        final Role childRole = createRoleIfNotFound("ROLE_CHILD", childPrivileges);
        final Role therapistRole = createRoleIfNotFound("ROLE_THERAPIST", therapistPrivileges);
        // == create initial user
        createUserIfNotFound("admin@test.com", "Admin", "Admin", "test", new ArrayList<Role>(Arrays.asList(adminRole)));
        createUserIfNotFound("testuser@test.com", "Child", "Test", "test", new ArrayList<Role>(Arrays.asList(childRole)));
        createUserIfNotFound("testterapist@test.com", "Terapeut", "Test", "test", new ArrayList<Role>(Arrays.asList(therapistRole)));
        createUserIfNotFound("testparinte@test.com", "Parinte", "Test", "test", new ArrayList<Role>(Arrays.asList(childRole)));
        alreadySetup = true;
    }

    @Transactional
    private final Privilege createPrivilegeIfNotFound(final String name) {
        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilege = privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    private final Role createRoleIfNotFound(final String name, final Collection<Privilege> privileges) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
        }
        role.setPrivileges(privileges);
        role = roleRepository.save(role);
        return role;
    }

    @Transactional
    private final User createUserIfNotFound(final String email, final String firstName, final String lastName, final String password, final Collection<Role> roles) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setPassword(passwordEncoder.encode(password));
            user.setEmail(email);
            user.setEnabled(true);
        }
        user.setRoles(roles);
        user = userRepository.save(user);
        return user;
    }

}
